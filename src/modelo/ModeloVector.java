/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class ModeloVector implements Modelo {

    Alumno alumnos[] = new Alumno[100];
    Alumno vacio = new Alumno("", "", 0, "");
    int id = 0;

    public ModeloVector() {
        for (int i = 0; i < alumnos.length; i++) {
            alumnos[i] = vacio;
        }
    }

    @Override
    public void create(Alumno alumno) {
        alumno.setId(id + "");
        alumnos[id] = alumno;
        id++;

    }

    @Override
    public Alumno read(String id) {

        Alumno alumno = null;
        int i = 0;
        while (i < alumnos.length) {
            alumno = alumnos[i];
            if (id.equals(alumno.getId())) {
                break;
            }
            i++;
        }
        return alumno;
    }

    @Override
    public void update(Alumno alumno) {

        int i = 0;
        while (i < alumnos.length) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = alumno;
            }
            i++;
        }

    }

    @Override
    public void delete(Alumno alumno) {
        int i = 0;
        while (i < alumnos.length) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = vacio;
            }
            i++;
        }
    }

}
