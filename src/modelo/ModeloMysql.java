package modelo;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author paco
 */
public class ModeloMysql implements Modelo {

    private static String bd = "ceedprgt8";
    private static String user = "alumno";
    private static String pass = "alumno";
    private Connection con = null;
    private Statement st = null;
    private ResultSet rs = null;
    private int id = 0;

    public ModeloMysql() {

    }

    public void desconectar() {
        try {
            System.out.println("BDR Mysql Connexión cerrada");
            con.close();
        } catch (SQLException ex) {

        }
    }

    public void conectar() {

        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            //System.out.println("Driver Registrado correctamente");
            //Abrir la conexion con la Base de Datos
            //System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/" + bd;
            con = (Connection) DriverManager.getConnection(jdbcUrl, user, pass);
            System.out.println("Conexion establecida con la Base de datos...");

        } catch (SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Errores debidos al Class.forName
            e.printStackTrace();
        }//end try

    }

    @Override
    public void create(Alumno alumno) {
        int resultado;
        int id_;
        String sql;

        try {
            conectar();
            st = con.createStatement();

            String nombre = alumno.getNombre();
            int edad = alumno.getEdad();
            String email = alumno.getEmail();

            sql = "insert into alumnos(nombre,edad,email) "
                    + "values('" + nombre + "','" + edad + "','" + email + "')";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);

            sql = "select max(id) as maxid from alumnos;";
            System.out.println(sql);
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                id = rs.getInt("maxid");
            }
            alumno.setId(id + "");
            desconectar();
        } catch (SQLException ex) {
            System.out.println("Error");
        }
    }

    @Override
    public Alumno read(String id_) {
        Alumno alumno = null;
        Boolean encontrado = false;

        try {

            conectar();
            st = con.createStatement();

            //Ejecutamos la SELECT sobre la tabla alumnos
            String sql = "select id,nombre,edad,email from alumnos where id=" + id_;
            System.out.println(sql);

            rs = st.executeQuery(sql);

            if (rs.next()) {
                String id;
                String nombre;
                int edad;
                String email;

                id = "" + id_;
                nombre = rs.getString("nombre");
                edad = rs.getInt("edad");
                email = rs.getString("email");
                alumno = new Alumno(id, nombre, edad, email);
                desconectar();
            }

        } catch (SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Errores de Class.forNameCliente
            e.printStackTrace();
        }
        return alumno;
    }

    @Override
    public void update(Alumno alumno) {
        int resultado;
        String sql;

        try {
            conectar();
            st = con.createStatement();

            sql = "update alumnos"
                    + " set nombre = " + alumno.getNombre()
                    + " set edad = " + alumno.getEdad()
                    + " set email = " + alumno.getEmail()
                    + " where id="
                    + alumno.getId()
                    + ";";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);

            desconectar();
        } catch (SQLException ex) {

        }
    }

    @Override
    public void delete(Alumno alumno) {
        int resultado;
        String sql;
        try {
            conectar();
            st = con.createStatement();

            sql = "delete from alumnos where id="
                    + alumno.getId()
                    + ";";
            System.out.println(sql);
            resultado = st.executeUpdate(sql);
            desconectar();
        } catch (SQLException ex) {

        }
    }

}
