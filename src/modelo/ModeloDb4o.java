package modelo;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author paco
 */
public class ModeloDb4o implements Modelo {

    private File fs;
    private int id = 0;
    private static String fichero = "alumnos.db40";
    private ObjectContainer bd;

    private int calculaid() {
        int idmax = 0;

        Alumno vacio = new Alumno(null, null, 0, null);
        Alumno a = null;
        String id;

        conectar();
        ObjectSet res = bd.queryByExample(vacio);
        while (res.hasNext()) {
            a = (Alumno) res.next();
            id = a.getId();
            if (Integer.parseInt(id) > idmax) {
                idmax = Integer.parseInt(id);
            }
        }
        desconectar();
        return idmax + 1;

    }

    public ModeloDb4o() throws IOException {
        fs = new File(fichero);
        id = calculaid();
    }

    public void conectar() {
        System.out.println("BDOO Connexión establecida");
        bd = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), fichero);
    }

    public void desconectar() {
        System.out.println("BDR Mysql Connexión cerrada");
        bd.close();
    }

    @Override
    public void create(Alumno alumno) {

        conectar();
        alumno.setId(id + "");
        System.out.println("Insertado Cliente " + alumno.toString());
        id++;
        bd.store(alumno);
        desconectar();
    }

    @Override
    public Alumno read(String id_) {

        Alumno alumno = null;
        Alumno buscado = new Alumno(id_, null, 0, null);

        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        if (res.hasNext()) {
            alumno = (Alumno) res.next();
        }
        desconectar();

        return alumno;
    }

    @Override
    public void update(Alumno alumno_) {

        Alumno alumno = null;
        Alumno buscado = new Alumno(alumno_.getId(), null, 0, null);

        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        if (res.hasNext()) {
            alumno = (Alumno) res.next();
            System.out.println("Update " + alumno.toString());
            alumno.setNombre(alumno_.getNombre());
            alumno.setEdad(alumno_.getEdad());
            alumno.setEmail(alumno_.getEmail());
            bd.store(alumno);
        }
        desconectar();

    }

    @Override
    public void delete(Alumno alumno_) {

        Alumno alumno = null;
        Alumno buscado = new Alumno(alumno_.getId(), null, 0, null);

        conectar();
        ObjectSet res = bd.queryByExample(buscado);
        if (res.hasNext()) {
            alumno = (Alumno) res.next();
            System.out.println("Delete " + alumno.toString());
            bd.delete(alumno);
        }
        desconectar();

    }

}
