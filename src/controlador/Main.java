/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import modelo.Modelo;
import modelo.ModeloDb4o;
import vista.Vista;
import vista.VistaPantalla;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        //Modelo modelo = new ModeloVector();
        //Modelo modelo = new ModeloArrayList();
        //Modelo modelo = new ModeloHashSet();
        //Modelo modelo = new ModeloFichero();
        //Modelo modelo = new ModeloMysql();
        Modelo modelo = new ModeloDb4o();

        //Vista vista = new VistaTerminal();
        Vista vista = new VistaPantalla();

        Controlador c = new Controlador(modelo, vista);

    }

}
